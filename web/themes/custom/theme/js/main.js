jQuery(window).scroll(function(){
    var scroll = jQuery(window).scrollTop();
    if(scroll > 50){
        jQuery('#main-page-header .navbar').addClass('bg-white');
        jQuery('#main-page-header .navbar').removeClass('bg-primary');
        jQuery('#main-page-header .navbar-expand-lg .navbar-nav .nav-link').addClass('nav-link-custom');
    }else{
        jQuery('#main-page-header .navbar').addClass('bg-primary');
        jQuery('#main-page-header .navbar').removeClass('bg-white');
        jQuery('#main-page-header .navbar-expand-lg .navbar-nav .nav-link').removeClass('nav-link-custom');
    }
});



//jQuery("#edit-submit").addClass("btn btn-primary btn-block");

jQuery('.owl-carousel').owlCarousel({
    autoplay: true,
    autoplayHoverPause: true,
    autoplayTimeout: 1000,
    nav: true,
    loop: true,
    responsive: {
        0:{
            items: 1,
            dots: false
        },
        485:{
            items: 2,
            dots: false
        },
        728:{
            items: 3,
            dots: true
        },
        960:{
            items: 4,
            dots: true
        },
        1200:{
          
            items: 5,
            dots: false,
            autoplay: false,
            autoplayHoverPause: false,
            autoplayTimeout: 1000,
            nav: false,
            loop: false,
           
        },
    }

});

new fullpage('#fullpage', {
    sectionsColor: ['yellow', 'orange', '#C0C0C0', '#ADD8E6'],
    navigation: true,
    controlArrows: true,
  });